
package com.infodalesRitesh.aem.core.models;


import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;

import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;


import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.beans.beancontext.BeanContextSupport;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Model(adaptables = Resource.class)

public class PageHerarchyModel {


    private List<String> childrenPagesList;

    public List<String> getChildrenPagesList()
    {
        return childrenPagesList;
    }

    @Self
    Resource resource;

    @PostConstruct
    protected void init(){
        childrenPagesList=new ArrayList<String>();

        Resource pageResource=resource.getParent();
        Page page=pageResource.adaptTo(Page.class);
        Iterator<Page> child= page.listChildren();
        while(child.hasNext()){
           Page c= child.next();
            childrenPagesList.add(c.getTitle());
        }

    }



}


