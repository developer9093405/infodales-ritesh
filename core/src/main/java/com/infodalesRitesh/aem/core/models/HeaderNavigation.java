package com.infodalesRitesh.aem.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = HeaderNavigation.class)
public class HeaderNavigation {

    @Inject
    private String label;

    @Inject
    private String link;

    public String getLabel() {
        return label;
    }

    public String getLink() {
        return link;
    }
    @Inject
    List<SubMenuLevelOne> subMenuLevelOne;

    public List<SubMenuLevelOne> getSubMenuLevelOne() {
        return subMenuLevelOne;
    }
}
