package com.infodalesRitesh.aem.core.servlets;

import com.infodalesRitesh.aem.core.service.MessageService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

@Component(service= Servlet.class,
        property={
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths="+"/bin/riteshJson",
                "sling.servlet.extensions=" + "txt"
        })
public class ReadTheNode extends SlingAllMethodsServlet {

    @Reference
    public MessageService messageService;

    @Override
    protected void doGet(SlingHttpServletRequest req,
                         SlingHttpServletResponse resp) throws ServletException, IOException
    {
        String path = "/content/dam/infodales-ritesh/Test";
        ResourceResolver resolver = req.getResourceResolver();
        Resource resource = resolver.getResource(path);

        Node node = resource.adaptTo(Node.class);

        String s1 = "";
        String s2 = "";
        String[] s3 = null;
        boolean s4 = false;

        try {
            if (node.hasProperty("name")) {
                s1 = node.getProperty("name").getString();
            }

            if (node.hasProperty("robot")) {
                s4 = node.getProperty("robot").getBoolean();
            }

            if (node.hasProperty("mobileno")) {
                s2 = node.getProperty("mobileno").getString();
            }

            if (node.hasProperty("boys")) {
                Property property = node.getProperty("boys");
                if (property.isMultiple()) {
                    Value[] s = property.getValues();
                    s3 = new String[s.length];
                    for (int i = 0; i < s.length; i++) {
                        s3[i] = s[i].getString();
                    }
                } else {
                    s3 = new String[1];
                    s3[0] = property.getString();
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        JSONObject js = new JSONObject();
        try {
            js.put("name", s1);
            js.put("robot", s4);
            js.put("mobileno", s2);
            js.put("boys", s3);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        resp.setContentType("application/js");
        resp.setContentType("text/plain");
        resp.setCharacterEncoding("utf-8");
        resp.getWriter().write(js.toString());

        resp.getWriter().write(messageService.showMsg());


    }
}