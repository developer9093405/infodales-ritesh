package com.infodalesRitesh.aem.core.models;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
public class SendMail {
    public static void main(String args[]){
        String to = "ankitpardhi101@gmail.com";

        // Sender's email ID needs to be mentioned
        String from = "ankitpardhi1012@gmail.com";

        // Assuming you are sending email from through gmails smtp
        String host = "192.168.0.186";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "25");

        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties);

        try {

            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);


            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("This is the Subject Line!");

            message.setContent("<h1>This is an actual message</h1>","text/plain");
            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }

    }

}
