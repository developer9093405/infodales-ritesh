package com.infodalesRitesh.aem.core.models;


import com.adobe.cq.dam.cfm.ContentFragment;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters = CfDemo.class)
public class CfDemo {

    @Inject
    private List<String> link;

    @Inject
    ResourceResolver resolver;

    private List<CFDemoPojo>listcf;
    public List<CFDemoPojo> getListcf() {
        return listcf;
    }

    @PostConstruct
    protected void init(){

        if (link!=null){
            listcf=new ArrayList<CFDemoPojo>();
            for (String s:link){

                CFDemoPojo cf=new CFDemoPojo();
                Resource fragmentResource = resolver.getResource(s);

                ContentFragment contentFragment = fragmentResource.adaptTo(ContentFragment.class);

                cf.setTitle(contentFragment.getElement("title").getContent().toString());
                cf.setDescription(contentFragment.getElement("description").getContent().toString());
                listcf.add(cf);
            }
        }
    }


}
