package com.infodalesRitesh.aem.core.service;

import com.infodalesRitesh.aem.core.beans.MovieReviewPojo;
import org.json.JSONObject;

import java.util.List;

public interface MovieReviewService {
    String getReviews(String movieTitle);

    List<MovieReviewPojo> getReviewsList(String movieTitle);
}
