package com.infodalesRitesh.aem.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class InTheaterLinkPojo {

    @Inject
    private String moviePageLink;

    public String getMoviePageLink() {
        return moviePageLink;
    }

    public void setMoviePageLink(String moviePageLink) {
        this.moviePageLink = moviePageLink;
    }
}
