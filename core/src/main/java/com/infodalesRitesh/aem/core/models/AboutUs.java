package com.infodalesRitesh.aem.core.models;;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Model(adaptables = Resource.class)
public class AboutUs {

    @Inject
    private String text;
    @Inject
    private String label;
    @Inject
    private String select;

    @Inject
    private String fileReference;
    private String message;

    public String getText() {
        return text;
    }

    public String getLabel() {
        return label;
    }

    public String getSelect() {
        return select;
    }

    public String getFileReference() {
        return fileReference;
    }

    public String getMessage()
    {
        return "this is aem test message for practice";
    }

}
