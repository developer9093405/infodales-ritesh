package com.infodalesRitesh.aem.core.servlets;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
@Component(service = Servlet.class,
        property = {
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.paths=/bin/infodales/registrationform"
        })
public class RegistrationServlet  extends SlingAllMethodsServlet {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationServlet.class);
    private static final long serialVersionUID = 1L;
    @Override
    protected void doPost(final SlingHttpServletRequest request,
                          final SlingHttpServletResponse response) throws IOException
    {
        int uid = Integer.parseInt(request.getParameter("uid"));
        String email=request.getParameter("email");
        String password=request.getParameter("password");
        String firstName=request.getParameter("firstName");
        String lastName=request.getParameter("lastName");
        Long phoneNumber= Long.valueOf(request.getParameter("phoneNumber"));
        String gender=request.getParameter("gender");
        String dob=request.getParameter("dob");
        String country=request.getParameter("country");
        int subscription_id=Integer.parseInt(request.getParameter("subscription_id"));
        CloseableHttpClient httpClient= HttpClients.createDefault();
        JSONObject jsonRequestBody= new JSONObject();
        try {
            jsonRequestBody.put("uid", uid);
            jsonRequestBody.put("email",email);
            jsonRequestBody.put("password",password);
            jsonRequestBody.put("firstName",firstName);
            jsonRequestBody.put("lastName",lastName);
            jsonRequestBody.put("phoneNumber",phoneNumber);
            jsonRequestBody.put("gender",gender);
            jsonRequestBody.put("dob",dob);
            jsonRequestBody.put("country",country);
            jsonRequestBody.put("subscription_id",subscription_id);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        String url ="http://192.168.0.139:8080/insert";
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-Type", "application/json");
        httpPost.setHeader("Accept", "application/json");
        StringEntity entity = new StringEntity(jsonRequestBody.toString());
        httpPost.setEntity(entity);
        HttpResponse httpResponse = httpClient.execute(httpPost);
        HttpEntity httpEntity = httpResponse.getEntity();
        response.setContentType("application/json");
        String responseContent = EntityUtils.toString(httpEntity);
        if(httpResponse.getStatusLine().getStatusCode()== HttpStatus.SC_BAD_REQUEST&&
                responseContent.equals("Profile Already exist"))
        {
            response.getWriter().write("Profile Already exist");
        }
        else
        {
            response.getWriter().write(responseContent);
            response.getWriter().write("Registration Successful");
        }
        httpClient.close();
    }
}
