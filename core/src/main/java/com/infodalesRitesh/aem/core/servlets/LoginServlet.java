package com.infodalesRitesh.aem.core.servlets;

import com.day.cq.commons.jcr.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

@Component(
        service= {Servlet.class},
        property={
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.resourceTypes=" +"infodales-ritesh/components/content/login",
                "sling.servlet.selectors=" + "login",
                "sling.servlet.extensions"+"="+"json"
        })
public class LoginServlet extends SlingAllMethodsServlet {
        private static final Logger logger = LoggerFactory.getLogger(LoginServlet.class);
        private static final long serialVersionUID = 1L;

        protected void doGet(final SlingHttpServletRequest req,
                             final SlingHttpServletResponse resp) throws ServletException, IOException
        {
                final Resource resource = req.getResource();
                resp.getWriter().write("created by = " + resource.getValueMap().get(JcrConstants.JCR_CREATED_BY));
        }

        protected void doPost(final SlingHttpServletRequest req,
                             final SlingHttpServletResponse resp) throws ServletException, IOException
        {
                       String loginId = req.getParameter("loginId");
                       String password = req.getParameter("password");
                        logger.info("User Id : "+ loginId);
                        logger.info("Password : "+ password);

                       resp.getWriter().write("login " + loginId+"\t password "+ password);
        }

}
