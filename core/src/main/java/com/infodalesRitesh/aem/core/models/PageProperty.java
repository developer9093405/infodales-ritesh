package com.infodalesRitesh.aem.core.models;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;

@Model(adaptables = Resource.class)
public class PageProperty {

    @Inject
    ResourceResolver resourceResolver;
    String title;

    @PostConstruct
    protected void init() throws Exception {
        String parentPath="/content/infodales-ritesh/assetpage";

        Resource resource=resourceResolver.getResource(parentPath);
        if(resource!=null){
            Page page=resource.adaptTo(Page.class);
            title=page.getTitle();
        }
    }
    public String getTitle() {
        return title;
    }
}
