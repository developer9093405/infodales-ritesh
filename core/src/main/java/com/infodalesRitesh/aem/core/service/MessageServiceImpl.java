package com.infodalesRitesh.aem.core.service;

import org.osgi.service.component.annotations.Component;

@Component(service=MessageService.class)
public class MessageServiceImpl implements MessageService {

    @Override
    public String showMsg() {
        return "Hi I am Infodales";
    }
}
