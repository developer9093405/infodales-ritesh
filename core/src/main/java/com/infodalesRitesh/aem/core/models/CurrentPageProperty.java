package com.infodalesRitesh.aem.core.models;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = SlingHttpServletRequest.class)
public class CurrentPageProperty {
    String title;
    @Inject
    SlingHttpServletRequest request;
    @Inject
    PageManager pageManager;

    @PostConstruct
    protected void init() throws Exception {
        Resource resource = request.getResource();
        Page page = pageManager.getContainingPage(resource);
        if (page != null) {
            title = page.getTitle();
        }
    }
    public String getTitle() {
        return title;
    }
}
