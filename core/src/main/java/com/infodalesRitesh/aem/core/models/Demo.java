package com.infodalesRitesh.aem.core.models;

import org.apache.sling.models.annotations.Model;
import org.apache.sling.api.resource.Resource;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
@Model(adaptables = Resource.class)
public class Demo {


    private ArrayList<String> employee;

    @PostConstruct
    protected void init(){
         employee =new ArrayList<String>();

        employee.add("Sunil");
        employee.add("Raju");
        employee.add("Vikas");
    }
    public ArrayList<String> getEmployee()
    {
        return employee;
    }


}
