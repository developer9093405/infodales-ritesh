package com.infodalesRitesh.aem.core.service;

import com.infodalesRitesh.aem.core.beans.IdPojo;

public interface UserIdService {

    public IdPojo getUserId(String userId);

}
