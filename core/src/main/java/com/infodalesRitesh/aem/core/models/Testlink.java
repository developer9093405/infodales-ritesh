package com.infodalesRitesh.aem.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, adapters = Testlink.class)

public class Testlink {



    private String pageTitle;

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
    public String getPageTitle() {
        return pageTitle;
    }


    private String link;

    public void setIconlink(String iconlink) {
        this.link = iconlink;
    }
    public String getIconlink() {
        return link;
    }

}
