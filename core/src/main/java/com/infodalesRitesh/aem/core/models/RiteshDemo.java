package com.infodalesRitesh.aem.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;

@Model(adaptables = Resource.class)
public class RiteshDemo {

    private String test;
    @PostConstruct
    protected void init(){
        test ="Hello World";
    }

    public String getTest() {
        return test;
    }
}
