package com.infodalesRitesh.aem.core.beans;

public class IdPojo {


    public IdPojo(String firstname, String lastname, String mobile_no, String email, String dob, String country) {
        Firstname = firstname;
        Lastname = lastname;
        this.mobile_no = mobile_no;
        this.email = email;
        this.dob = dob;
        this.country = country;


    }


    private String Firstname;
    private String Lastname;
    private String mobile_no;
    private String email;
    private String dob;
    private String country;


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String firstname) {
        Firstname = firstname;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

}
