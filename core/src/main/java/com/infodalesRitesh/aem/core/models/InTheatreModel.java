package com.infodalesRitesh.aem.core.models;

import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class InTheatreModel {

    @Inject
    List<InTheaterLinkPojo> inTheater;

    List<InTheaterPojo> inTheaterPojoList;

    @Inject
    ResourceResolver resourceResolver;
    @PostConstruct
    protected void init(){
        if(inTheater!=null) {
            inTheaterPojoList = new ArrayList<InTheaterPojo>();
            for (InTheaterLinkPojo link:inTheater){
                if(link!=null) {
                    Resource resource=resourceResolver.getResource(link.getMoviePageLink());
                    InTheaterPojo inTheaterPojo=new InTheaterPojo();
                    PageManager pm =resourceResolver.adaptTo(PageManager.class);
                    Page page=pm.getContainingPage(resource);
                    ValueMap pageproperties=page.getProperties();
                    inTheaterPojo.setTitle((String)pageproperties.get("jcr:title"));
                    inTheaterPojo.setTileimage((String)pageproperties.get("tileimagefield"));
                    inTheaterPojoList.add(inTheaterPojo);
                }
            }


        }

    }
    public List<InTheaterLinkPojo> getInTheater() {
        return inTheater;
    }

    public List<InTheaterPojo> getInTheaterPojoList() {
        return inTheaterPojoList;
    }
}
