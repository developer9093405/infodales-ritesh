package com.infodalesRitesh.aem.core.models;

import javax.inject.Inject;

public class AssetExamplePojo {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
