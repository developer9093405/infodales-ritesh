package com.infodalesRitesh.aem.core.servlets;

import org.apache.sling.api.SlingHttpServletRequest;

import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component(
        service= {Servlet.class},
        property={
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths=" +"/bin/infodales/getsession",
        })
public class GetSessionServlet extends SlingSafeMethodsServlet {

    private static final Logger logger = LoggerFactory.getLogger(GetSessionServlet.class);
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final SlingHttpServletRequest req,
                         final SlingHttpServletResponse resp) throws ServletException, IOException
    {
        String value="";

        HttpSession session = req.getSession();
        if(session != null) {
            Object sessionOb = session.getAttribute("user");
            if (sessionOb != null) {
                value = sessionOb.toString();
            } else {
                value = "Session is empty";
            }

        }else
        {
            value="Session is not created";
        }
        resp.getWriter().write(value);



    }
}
