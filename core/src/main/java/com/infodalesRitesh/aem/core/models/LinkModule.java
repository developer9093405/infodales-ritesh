package com.infodalesRitesh.aem.core.models;


import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters = LinkModule.class)
public class LinkModule {

    @Inject
    private String link;

    @Inject
    private String fileReference;

    @Inject
    private String alttext;

    @Inject
    private List<NewLink> myMultifield;

    public String getLink() {
        return link;
    }

    public String getFileReference() {
        return fileReference;
    }

    public String getAlttext() {
        return alttext;
    }

    public List<NewLink> getMyMultifield() {
        return myMultifield;
    }



}