package com.infodalesRitesh.aem.core.models;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters=PageLinkDemo.class)
public class PageLinkDemo {

    @Inject
    private String label;

    @Inject
    private String description;

    @Inject
    private ResourceResolver resolver;

    @Inject
    private List<String> link;

    private List<DemoPagePojo> linklist;

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    public List<DemoPagePojo> getLinklist() {
        return linklist;
    }

    @PostConstruct
    protected void init(){

        if(link!=null){
            linklist=new ArrayList<DemoPagePojo>();
            for(String li:link){
                DemoPagePojo dp=new DemoPagePojo();
                Resource resource =resolver.getResource(li);
                Page page=resource.adaptTo(Page.class);
                dp.setLink(li);
                dp.setPageTitle(page.getTitle());
                linklist.add(dp);
            }
        }
    }


}
