package com.infodalesRitesh.aem.core.service;


import com.google.gson.Gson;
import com.infodalesRitesh.aem.core.beans.MovieReviewPojo;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;

import java.util.ArrayList;
import java.util.List;

@Component(service= MovieReviewService.class,immediate = true)
public class MovieReviewServiceImpl implements MovieReviewService {

    @Override
    public String getReviews(String movieTitle) {
        String json=null;
        try {
            String url = String.format("http://192.168.0.139:8080/getReview?movieTitle="+movieTitle);

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpGet httpGet = new HttpGet(url);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            json = EntityUtils.toString(httpResponse.getEntity());
            JSONObject jsonObject = new JSONObject(json);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return json;
    }

    @Override
    public List<MovieReviewPojo> getReviewsList(String movieTitle){
        List<MovieReviewPojo> reviewList = new ArrayList<>();
        try {
            String json = getReviews(movieTitle);
            JSONObject jsonObject = new JSONObject(json);
            JSONArray reviewArray = jsonObject.getJSONArray("review");
            for (int i=0; i<reviewArray.length(); i++) {
                JSONObject reviewObject = reviewArray.getJSONObject(i);
                MovieReviewPojo movieReviewPojo = new MovieReviewPojo();
                movieReviewPojo.setMovieTitle(reviewObject.getString("title"));
                movieReviewPojo.setUserMovieRating(reviewObject.getDouble("rating"));
                movieReviewPojo.setUserMovieReview(reviewObject.getString("review"));
                reviewList.add(movieReviewPojo);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return reviewList;
    }

}

