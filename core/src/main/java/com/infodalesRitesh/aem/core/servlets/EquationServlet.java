package com.infodalesRitesh.aem.core.servlets;

import com.day.cq.commons.jcr.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

@Component(
        service= {Servlet.class},
property={
        "sling.servlet.methods=" + HttpConstants.METHOD_GET,
        "sling.servlet.resourceTypes=" +"infodales-ritesh/components/structure/equationcomponent",
//        "sling.servlet.selectors=" + "info1",
//        "sling.servlet.selectors=" + "info2",

//        "sling.servlet.extensions"+"="+"txt"
        })

public class EquationServlet extends SlingSafeMethodsServlet {
    private static final Logger logger = LoggerFactory.getLogger(EquationServlet.class);
    private static final long serialVersionUID = 1L;
    protected void doGet(final SlingHttpServletRequest req,
                         final SlingHttpServletResponse resp) throws ServletException, IOException
    {
        final Resource resource = req.getResource();
        String value = resource.getValueMap().get("value").toString();
        logger.info("value of string >> "+ value);
        String s = value.toUpperCase();
        logger.info("value of string in uppercase >> "+ s);
        resp.setContentType("text/plain");
        resp.getWriter().write("Title = " + resource.getValueMap().get(JcrConstants.JCR_TITLE));
        resp.getWriter().write("\n ValueInUppercase = " + s );

    }

}
