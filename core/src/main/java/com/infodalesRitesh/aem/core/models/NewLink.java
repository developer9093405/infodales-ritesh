package com.infodalesRitesh.aem.core.models;

import org.apache.sling.api.resource.Resource;

import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, adapters = NewLink.class)
public class NewLink {

    @Inject
    private String element;

    public String getElement() {
        return element;
    }

    @Inject
    private String link;

    public String getLink() {
        return link;
    }


















}
