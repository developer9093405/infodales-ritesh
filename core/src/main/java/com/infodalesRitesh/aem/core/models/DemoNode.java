package com.infodalesRitesh.aem.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

@Model(adaptables = Resource.class)
public class DemoNode {

    @Inject
    ResourceResolver resourceResolver;


    @PostConstruct
    protected void init() throws RepositoryException {
        String parentPath="/content/dam/infodales-rohit";
        Resource resource=resourceResolver.getResource(parentPath);
        if(resource!=null){
            Node parentnode=resource.adaptTo(Node.class);
            if(parentnode!=null && !parentnode.hasNode("label")){
                Node newNode=parentnode.addNode("label","nt:unstructured");
                newNode.setProperty("name","World is here");
                newNode.setProperty("mobileno","123456788");
                newNode.setProperty("robot",false);
                newNode.setProperty("boys","ritesh,ankit,Rohit,owais,sahbaz");
                newNode.getSession().save();
            }
        }

    }
}
