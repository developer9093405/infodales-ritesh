package com.infodalesRitesh.aem.core.service;

import com.infodalesRitesh.aem.core.beans.IdPojo;
import org.osgi.service.component.annotations.Component;

import java.util.HashMap;

@Component(service=UserIdService.class,immediate = true)
public class UserIdServiceImpl implements UserIdService{


    @Override
    public IdPojo getUserId(String userId) {

        IdPojo user1 = new IdPojo ("Ritesh", "Aware", "3567888934", "ritesh.infodales@gmail.com", "null", "India");
        IdPojo user2= new IdPojo("Vicky", "Thakur", "3567888934", "vicky.infodales@gmail.com", "null", "India");
        IdPojo user3=new IdPojo("Sahbaz", "Khan", "3567888934", "Sahbaz.infodales@gmail.com", "null", "India");
        IdPojo user4=new IdPojo("Ankit", "Pardhi", "3567888934", "ankit.infodales@gmail.com", "null", "India");
        IdPojo user5= new IdPojo("Owais", "Pathan", "3567888934", "owais.infodales@gmail.com", "null", "India");

        HashMap<String,IdPojo> hm=new HashMap<String,IdPojo>();
        hm.put("Ritesh",user1);
        hm.put("Vicky",user2);
        hm.put("Sahbaz",user3);
        hm.put("Ankit",user4);
        hm.put("Owais",user5);


        return hm.get(userId);
    }
}
