package com.infodalesRitesh.aem.core.beans;

public class BranchesPojo {
    int id;
    String name;
    int code;
    String locate;
    String published_at;
    String stt;
    String unitCode;
    String shortName;
    String zone;
    String area;
    String region;
    String CNME;
    String CNNHNN;
    String street;
    String NHNNArchitecture;
    String status;
    String internalArchitecture;
    String phone;
    String address;
    float lat;
    float Long;
    String webName;
    String[] localizations;

    public BranchesPojo(int id, String name, int code, String locate, String published_at, String stt, String unitCode, String shortName, String zone, String area, String region, String CNME, String CNNHNN, String street, String NHNNArchitecture, String status, String internalArchitecture, String phone, String address, float lat, float aLong, String webName, String[] localizations) {

        this.id = id;
        this.name = name;
        this.code = code;
        this.locate = locate;
        this.published_at = published_at;
        this.stt = stt;
        this.unitCode = unitCode;
        this.shortName = shortName;
        this.zone = zone;
        this.area = area;
        this.region = region;
        this.CNME = CNME;
        this.CNNHNN = CNNHNN;
        this.street = street;
        this.NHNNArchitecture = NHNNArchitecture;
        this.status = status;
        this.internalArchitecture = internalArchitecture;
        this.phone = phone;
        this.address = address;
        this.lat = lat;
        Long = aLong;
        this.webName = webName;
        this.localizations = localizations;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLocate() {
        return locate;
    }

    public void setLocate(String locate) {
        this.locate = locate;
    }

    public String getPublished_at() {
        return published_at;
    }

    public void setPublished_at(String published_at) {
        this.published_at = published_at;
    }

    public String getStt() {
        return stt;
    }

    public void setStt(String stt) {
        this.stt = stt;
    }

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCNME() {
        return CNME;
    }

    public void setCNME(String CNME) {
        this.CNME = CNME;
    }

    public String getCNNHNN() {
        return CNNHNN;
    }

    public void setCNNHNN(String CNNHNN) {
        this.CNNHNN = CNNHNN;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNHNNArchitecture() {
        return NHNNArchitecture;
    }

    public void setNHNNArchitecture(String NHNNArchitecture) {
        this.NHNNArchitecture = NHNNArchitecture;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInternalArchitecture() {
        return internalArchitecture;
    }

    public void setInternalArchitecture(String internalArchitecture) {
        this.internalArchitecture = internalArchitecture;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLong() {
        return Long;
    }

    public void setLong(float aLong) {
        Long = aLong;
    }

    public String getWebName() {
        return webName;
    }

    public void setWebName(String webName) {
        this.webName = webName;
    }

    public String[] getLocalizations() {
        return localizations;
    }

    public void setLocalizations(String[] localizations) {
        this.localizations = localizations;
    }
}
