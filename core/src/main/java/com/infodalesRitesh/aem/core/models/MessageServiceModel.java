
package com.infodalesRitesh.aem.core.models;

import com.infodalesRitesh.aem.core.service.MessageService;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;

import javax.annotation.PostConstruct;

@Model(adaptables = Resource.class)
public class MessageServiceModel {

    @OSGiService
    private MessageService messageService;

    private String message;

    @PostConstruct
    protected void init(){
        message=messageService.showMsg();
    }
    public String getMessage() {
        return message;
    }
}
