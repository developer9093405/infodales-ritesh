package com.infodalesRitesh.aem.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = SubMenuLevelOne.class)
public class SubMenuLevelOne {

    @Inject
    private String label;

    @Inject
    private String link;

    @Inject
    List<SubMenuLevelTwo> subMenuLevelTwo;

    public String getLabel() {
        return label;
    }

    public String getLink() {
        return link;
    }

    public List<SubMenuLevelTwo> getSubMenuLevelTwo() {
        return subMenuLevelTwo;
    }
}
