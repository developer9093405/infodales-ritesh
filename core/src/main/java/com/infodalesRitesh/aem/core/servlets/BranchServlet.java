package com.infodalesRitesh.aem.core.servlets;

import com.infodalesRitesh.aem.core.beans.BranchesPojo;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.ContentFragmentException;
import com.adobe.cq.dam.cfm.FragmentTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONArray;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

import java.util.*;

import javax.servlet.Servlet;

@Component(service = Servlet.class,
        property = {
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths=" + "/bin/branch"
        })
public class BranchServlet extends SlingSafeMethodsServlet {

        private static final long serialVersionUID = 1L;
        static Logger log = Logger.getLogger(BranchServlet.class);
        BranchesPojo branch;
        List<BranchesPojo> branchesList;

        @Reference
        ResourceResolverFactory resourceResolverFactory;

        @Override
        protected void doGet(final SlingHttpServletRequest req,
                             final SlingHttpServletResponse resp) throws ServletException, IOException {
                HttpGet request = new HttpGet("https://techcombank.com/api/data/branches");
                request.addHeader("accept", "application/json");
                request.setHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 Firefox/26.0");
                CloseableHttpClient httpClient = HttpClients.createDefault();
                JSONArray jsonarray = null;
                CloseableHttpResponse response = httpClient.execute(request);
                branchesList = new ArrayList<BranchesPojo>();
                try {
                        HttpEntity entity = response.getEntity();
                        if (entity != null) {
                                String responseJson = EntityUtils.toString(entity);
                                Gson gson = new Gson();
                                BranchesPojo[] arrBranches = gson.fromJson(responseJson, BranchesPojo[].class);
                                List<BranchesPojo> branchesList = Arrays.asList(arrBranches);

                                Resource resource1 = req.getResource();
                                ResourceResolver resourceResolver = resource1.getResourceResolver();
                                Resource cfModel = resourceResolver.getResource("/conf/ContentFragmentWriter/settings/dam/cfm/models/branches");

                                Resource parentRes = resourceResolver.getResource("/content/dam/contentfragmentwriterfolder");
                                FragmentTemplate template = cfModel.adaptTo(FragmentTemplate.class);

                                for (BranchesPojo branches1 : branchesList) {
                                        ContentFragment atmFragment = template.createFragment(parentRes, branches1.getName(), branches1.getName());
                                        resourceResolver.commit();
                                        ObjectMapper objectMapper = new ObjectMapper();
                                        Map<String,String> mapTest=objectMapper.convertValue(branches1, Map.class);

                                        Iterator<ContentElement> iter = atmFragment.getElements();
                                        while (iter.hasNext()) {
                                                ContentElement childCF = iter.next();
                                                if (StringUtils.isNotBlank(childCF.getName())) {
                                                        childCF.setContent(mapTest.get(childCF.getName()), mapTest.get(childCF.getName()));
                                                }
                                        }
                                        resourceResolver.commit();
                                }
                        }
                } catch (ContentFragmentException | IOException e) {
                        log.info("Author content creation failure : " + e.getMessage());
                } finally {
                        httpClient.close();
                        response.close();
                }
                resp.setContentType("text/plain");
                resp.getWriter().write(" node added succesfully");
        }
}
