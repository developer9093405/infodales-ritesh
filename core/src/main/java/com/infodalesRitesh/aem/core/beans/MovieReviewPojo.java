package com.infodalesRitesh.aem.core.beans;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.Generated;


public class MovieReviewPojo {

    private String email;
    private String movieTitle;
    private Double userMovieRating;
    private String userMovieReview;
    private String dateAndTime;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public Double getUserMovieRating() {
        return userMovieRating;
    }

    public void setUserMovieRating(Double userMovieRating) {
        this.userMovieRating = userMovieRating;
    }

    public String getUserMovieReview() {
        return userMovieReview;
    }

    public void setUserMovieReview(String userMovieReview) {
        this.userMovieReview = userMovieReview;
    }

    public String getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }


}