package com.infodalesRitesh.aem.core.servlets;

import com.google.gson.JsonObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component(
        service= {Servlet.class},
        property={
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.paths=" +"/bin/infodales/loginpage"
        })

public class LoginPageServlet extends SlingAllMethodsServlet {

    private static final Logger logger = LoggerFactory.getLogger(LoginPageServlet.class);
    private static final long serialVersionUID = 1L;
    @Override
    protected void doPost(final SlingHttpServletRequest request,
                          final SlingHttpServletResponse response) throws IOException{
        String email=request.getParameter("email");
        String password=request.getParameter("password");
        CloseableHttpClient httpClient=HttpClients.createDefault();
        JSONObject json=new JSONObject();
        try {
            json.put("email",email);
            json.put("password",password);

        }catch(JSONException e){
            throw new RuntimeException(e);
        }
        String url ="http://192.168.0.139:8080/login";
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-Type", "application/");
        httpPost.setHeader("Accept", "application/json");
        StringEntity entity=new StringEntity(json.toString());
        httpPost.setEntity(entity);
        HttpResponse httpResponse= httpClient.execute(httpPost);
        HttpEntity httpEntity = httpResponse.getEntity();
        response.setContentType("application/json");
        String responseContent = EntityUtils.toString(httpEntity);
        if(httpResponse.getStatusLine().getStatusCode()== HttpStatus.SC_BAD_REQUEST&&responseContent.equals("Invalid login"))
        {
            response.getWriter().write("Invalid login");
        }else if(httpResponse.getStatusLine().getStatusCode()== HttpStatus.SC_OK){
            String token = responseContent;

            HttpSession session=request.getSession();
            session.setAttribute("email",token);
            response.getWriter().write("Login Successful");
            response.sendRedirect("");

        }else{
            responseContent.equals("Invalid entry");
        }
    }
}
