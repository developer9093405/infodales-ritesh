package com.infodalesRitesh.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(property = {
        Constants.SERVICE_DESCRIPTION + "= Infodales Workflow Model Process Step",
        Constants.SERVICE_VENDOR + "=InfoDales ",
        "process.label" + "=Infodales Workflow Model Process Step"
})
public class InfodalesWorkflowModel implements WorkflowProcess {

    private static final Logger logger = LoggerFactory.getLogger(InfodalesWorkflowModel.class);
    @Override
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap) throws WorkflowException {
        logger.info("In Infodales Workflow Model"+ workItem.getContentPath());
    }
}
