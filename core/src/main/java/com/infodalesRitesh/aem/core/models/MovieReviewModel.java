package com.infodalesRitesh.aem.core.models;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.infodalesRitesh.aem.core.beans.MovieReviewPojo;
import com.infodalesRitesh.aem.core.service.MovieReviewService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

@Model(adaptables = SlingHttpServletRequest.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = MovieReviewModel.class)
public class MovieReviewModel {

    @Inject
    private String review;
    @SlingObject
    SlingHttpServletRequest request;
    @OSGiService
    MovieReviewService movieReviewService;
    @Self
    Resource resource;
    @Inject
    PageManager pageManager;
    List<MovieReviewPojo> movieReviewPojoList;
    String title;
    @PostConstruct
    protected void init(){
        Resource resource = request.getResource();
        Page page = pageManager.getContainingPage(resource);
        if (page != null) {
            title = page.getTitle();
        }
        movieReviewPojoList = movieReviewService.getReviewsList(title);


    }

    public String getReview() {
        return review;
    }
    public List<MovieReviewPojo> getMovieReviewPojoList() {
        return movieReviewPojoList;
    }
    public String getTitle() {
        return title;
    }
}
