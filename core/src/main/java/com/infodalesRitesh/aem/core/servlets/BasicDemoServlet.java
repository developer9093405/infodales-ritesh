package com.infodalesRitesh.aem.core.servlets;

import com.day.cq.commons.jcr.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

import static java.awt.SystemColor.text;

@Component(service= Servlet.class,
        property={
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths="+"infodales-ritesh/components/content/testcomponent",
//                "sling.servlet.extensions=" + "txt"
        })
public class BasicDemoServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(final SlingHttpServletRequest req,
                         final SlingHttpServletResponse resp) throws ServletException, IOException
    {
        final Resource resource = req.getResource();
        resp.setContentType("text/plain");
       resp.getWriter().write("Text  "+  resource.getValueMap().get("text"));

    }
}
