package com.infodalesRitesh.aem.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

@Model(adaptables = Resource.class)

public class NodeDamDemo {

    @Inject
    ResourceResolver resolver;

    @PostConstruct
    protected void init() throws RepositoryException {
        String parentPath="/content/dam/infodales-ritesh";
        Resource rresource=resolver.getResource(parentPath);
        if(rresource!=null){
            Node parentnode=rresource.adaptTo(Node.class);
            if(parentnode!=null && !parentnode.hasNode("Test")){
                Node newNode=parentnode.addNode("Test","nt:unstructured");
                newNode.setProperty("name","World");
                newNode.setProperty("mobileno","123456789");
                newNode.setProperty("robot",false);
                newNode.setProperty("boys","ritesh,ankit,Rohit");
                newNode.getSession().save();
            }
        }
    }
}
