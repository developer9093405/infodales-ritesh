package com.infodalesRitesh.aem.core.models;

import com.day.cq.dam.api.Asset;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters = AssetsExample.class)
public class AssetsExample {

    @Inject
    private List<String> assetlink;

    @Inject
    ResourceResolver resolver;

    public List<AssetExamplePojo>assetExamplePojoslist;


    public List<AssetExamplePojo> getAssetExamplePojoslist() {
        return assetExamplePojoslist;
    }

    @PostConstruct
    protected void init(){
        if(assetlink!=null){
            assetExamplePojoslist=new ArrayList<AssetExamplePojo>();
            for (String link:assetlink){
                AssetExamplePojo ae=new AssetExamplePojo();
                Resource resource=resolver.getResource(link);
                Asset asset=resource.adaptTo(Asset.class);

                ae.setTitle(asset.getMetadataValue("dc:title"));
                

                assetExamplePojoslist.add(ae);
            }
        }
    }



}
