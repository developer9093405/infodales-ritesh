<%--
  ADOBE CONFIDENTIAL
  ___________________

  Copyright 2013 Adobe
  All Rights Reserved.

  NOTICE: All information contained herein is, and remains
  the property of Adobe and its suppliers, if any. The intellectual
  and technical concepts contained herein are proprietary to Adobe
  and its suppliers and are protected by all applicable intellectual
  property laws, including trade secret and copyright laws.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe.
--%><%
%><%@ include file="/libs/granite/ui/global.jsp" %><%
%><%@ page import="java.util.HashMap,
                  org.apache.commons.lang3.StringUtils,
                  org.apache.sling.api.SlingHttpServletRequest,
                  org.apache.sling.api.wrappers.ValueMapDecorator,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.ComponentHelper,
                  com.adobe.granite.ui.components.ComponentHelper.Options,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Field,
                  com.adobe.granite.ui.components.FormData,
                  com.adobe.granite.ui.components.FormData.NameNotFoundMode,
                  com.adobe.granite.ui.components.Tag,
                  com.adobe.granite.ui.components.Value" %><%--###
Multifield
==========

.. granite:servercomponent:: /libs/granite/ui/components/foundation/form/multifield
   :supertype: /libs/granite/ui/components/foundation/form/field
   :deprecated:

   Multifield component allows to add/reorder/remove multiple instances of a field.
   In the simplest case this is a simple form input field (e.g. textfield, textarea) but it can also be a complex component acting as an aggregate of multiple subcomponents (e.g. address entry).

   The field to be included is defined in the subnode named ``field``.
   Regardless of the component used, Multifield assumes they all write to the same property (defined by the ``name`` property of the
   ``field`` subnode) thus the resulting value will always be multi value.

   When rendering existing entries, the Multifield will iterate over the multi value, get each value, set it in the
   field and let the field render it.

   For example if the field is a text field with a ``name`` value of "fruits", and you use Multifield to
   add three text fields and set them with values "apple", "orange" and "banana", the content node will get the following property after saving::

      fruits: ["apple", "orange", "banana"]

   If the field is a composite, it is its responsibility to concatenate the fields into one, before the form gets submitted, and to separate them again on rendering.

   It extends :granite:servercomponent:`Field </libs/granite/ui/components/foundation/form/field>` component.

   It has the following content structure:

   .. gnd:gnd::

      [granite:FormMultifield] > granite:FormField

      /**
       * The id attribute.
       */
      - id (String)

      /**
       * The class attribute. This is used to indicate the semantic relationship of the component similar to ``rel`` attribute.
       */
      - rel (String)

      /**
       * The class attribute.
       */
      - class (String)

      /**
       * The title attribute.
       */
      - title (String) i18n

      /**
       * Indicates if the field is mandatory to be filled.
       */
      - required (Boolean)

      /**
       * The name of the validator to be applied. E.g. ``foundation.jcr.name``.
       * See :doc:`validation </jcr_root/libs/granite/ui/components/foundation/clientlibs/foundation/js/validation/index>` in Granite UI.
       */
      - validation (String) multiple

      /**
       * ``true`` to generate the `SlingPostServlet @Delete <http://sling.apache.org/documentation/bundles/manipulating-content-the-slingpostservlet-servlets-post.html#delete>`_ hidden input based on the field name.
       */
      - deleteHint (Boolean) = true

      /**
       * The value of `SlingPostServlet @TypeHint <http://sling.apache.org/documentation/bundles/manipulating-content-the-slingpostservlet-servlets-post.html#typehint>`_.
       */
      - typeHint (String)

      /**
       * The actual field of the Multifield.
       */
      + field

   Example::

      + myinput
        - sling:resourceType = "granite/ui/components/foundation/form/multifield"
        + field
          - sling:resourceType = "granite/ui/components/foundation/form/textfield"
          - name = "./pages"
###--%><%

Config cfg = cmp.getConfig();
ValueMap vm = (ValueMap) request.getAttribute(Field.class.getName());

Tag tag = cmp.consumeTag();
AttrBuilder attrs = tag.getAttrs();

attrs.add("id", cfg.get("id", String.class));
attrs.addRel(cfg.get("rel", String.class));
attrs.addClass(cfg.get("class", String.class));
attrs.add("title", i18n.getVar(cfg.get("title", String.class)));

attrs.add("role", "listbox");
attrs.add("aria-multiselectable", true);

String fieldLabel = cfg.get("fieldLabel", String.class);
String fieldDesc = cfg.get("fieldDescription", String.class);
String labelledBy = null;

if (fieldLabel != null && fieldDesc != null) {
    labelledBy = vm.get("labelId", String.class) + " " + vm.get("descriptionId", String.class);
} else if (fieldLabel != null) {
    labelledBy = vm.get("labelId", String.class);
} else if (fieldDesc != null) {
    labelledBy = vm.get("descriptionId", String.class);
}

if (StringUtils.isNotBlank(labelledBy)) {
    attrs.add("aria-labelledby", labelledBy);
}

if (cfg.get("required", false)) {
    attrs.add("aria-required", true);
}

String validation = StringUtils.join(cfg.get("validation", new String[0]), " ");
attrs.add("data-validation", validation);

attrs.addClass("coral-Multifield");
attrs.add("data-init", "multifield");

attrs.addOthers(cfg.getProperties(), "id", "rel", "class", "title", "required", "validation", "fieldLabel", "fieldDescription", "renderReadOnly", "deleteHint");

Resource field = cfg.getChild("field");

Config fieldCfg = new Config(field);
String name = fieldCfg.get("name", String.class);
Object[] values = vm.get("value", new Object[0]); // don't convert; pass empty Object array as default value

%><div <%= attrs.build() %>><%
    if (cfg.get("deleteHint", true)) {
        AttrBuilder deleteAttrs = new AttrBuilder(request, xssAPI);
        deleteAttrs.add("type", "hidden");
        deleteAttrs.add("name", name + "@Delete");

        %><input <%= deleteAttrs.build() %>><%
    }

    String typeHint = cfg.get("typeHint", String.class);
    if (!StringUtils.isBlank(typeHint)) {
        AttrBuilder typeAttrs = new AttrBuilder(request, xssAPI);
        typeAttrs.add("type", "hidden");
        typeAttrs.add("name", name + "@TypeHint");
        typeAttrs.add("value", typeHint);

        %><input <%= typeAttrs %>><%
    }
    %><ol class="coral-Multifield-list js-coral-Multifield-list"><%
	    for (int i = 0; i < values.length; i++) {
	        %><li class="coral-Multifield-input js-coral-Multifield-input" role="option" aria-selected="true"><% include(field, name, values[i], cmp, slingRequest); %></li><%
	    }
    %></ol>
    <script class="js-coral-Multifield-input-template" type="text/html"><% include(field, cmp, slingRequest); %></script>
</div><%!

/**
 * Includes the field with no value set at all.
 */
@SuppressWarnings({ "deprecation", "null" })
private static void include(Resource field, ComponentHelper cmp, SlingHttpServletRequest request) throws Exception {
    FormData.push(request, new ValueMapDecorator(new HashMap<String, Object>()), NameNotFoundMode.IGNORE_FRESHNESS);

    // Setting the attributes below is superceded by FormData above, but maintained for compatibility
    ValueMap existingVM = (ValueMap) request.getAttribute(Value.FORM_VALUESS_ATTRIBUTE);
    String existingPath = (String) request.getAttribute(Value.CONTENTPATH_ATTRIBUTE);

    request.removeAttribute(Value.FORM_VALUESS_ATTRIBUTE);
    request.removeAttribute(Value.CONTENTPATH_ATTRIBUTE);

    cmp.include(field, new Options().rootField(false));

    FormData.pop(request);
    request.setAttribute(Value.FORM_VALUESS_ATTRIBUTE, existingVM);
    request.setAttribute(Value.CONTENTPATH_ATTRIBUTE, existingPath);
}

/**
 * Includes the field with the given values set.
 */
@SuppressWarnings({ "deprecation", "null" })
private static void include(Resource field, String name, Object value, ComponentHelper cmp, SlingHttpServletRequest request) throws Exception {
    ValueMap map = new ValueMapDecorator(new HashMap<String, Object>());
    map.put(name, value);

    FormData formData = FormData.from(request);
    NameNotFoundMode mode = NameNotFoundMode.IGNORE_FRESHNESS;
    if (formData != null) {
        mode = formData.getMode();
    }
    FormData.push(request, map, mode); // The mode is irrelevant actually, as the name-value pair is always set.

    // Setting the attribute below is superceded by FormData above, but maintained for compatibility
    ValueMap existing = (ValueMap) request.getAttribute(Value.FORM_VALUESS_ATTRIBUTE);
    request.setAttribute(Value.FORM_VALUESS_ATTRIBUTE, map);

    cmp.include(field, new Options().rootField(false));

    FormData.pop(request);
    request.setAttribute(Value.FORM_VALUESS_ATTRIBUTE, existing);
}
%>